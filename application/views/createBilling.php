<script type="text/javascript">
  $(document).ready(function(){
    function loaddata(){
      var myData;
      var s={"cmp":$("#cmp_s").val()}
      $.ajax({
          type:"POST",
          dataType:'json',
          url :"<?php echo base_url('billingstatement/getsupplier') ?>",
          data:s,

          success:function(data){
            myData = data

          },


          async: false
          });

          return myData;
    }

    $(' .ui.selection.dropdown.cmp').dropdown().change(function(){
  var $result = loaddata()

       $('.ui.search')
         .search({
           source: $result,
           fullTextSearch:'exact',

           onSelect: function (result,response) {
                  $("#sup_code_s").val(result.ID);
                  $("#Supplier_name_s").val(result.name);


                  return true;
         }


      });


    });

    $('.ui.calendar').calendar({
  type: 'date',
  formatter: {
     date: function (date, settings) {
       if (!date) return '';
       var day = date.getDate();
       var month = date.getMonth() + 1;
       var year = date.getFullYear();
       return  year + '-' + month + '-' + day ;
     }}
   });


   $('.ui.calendar.month').calendar({
 type: 'month',
 formatter: {
    date: function (date, settings) {
      if (!date) return '';
      var month = date.getMonth() + 1;
      var year = date.getFullYear();
      return  + year+ '-' + month + '-'+'20'  ;
    }}
});

    $("#search").click(function(){
      table.ajax.reload();
    })
  var table =  $('#table').DataTable({
            "ajax": {
              url: "<?php echo site_url('billingstatement/getAllListtocreate') ?>",
              type: 'POST',
              data: function ( d ) {
                    d.cmp=$("#cmp_s").val(),
                    d.sup_code=$("#sup_code_s").val(),
                    d.month=$("#month_s").val()
                }

            },

            "ordering": false,
            "aoColumnDefs": [ {
         "aTargets": [ 4,5,6,7 ],
       "mRender": function (data, type, full) {
        var formmatedvalue=data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
         return formmatedvalue;
       }
    },{
      "targets": -1,
      "data": null,
      "defaultContent": '<button id="delete" class="ui  icon red button delete "><i class="window close Square icon"></i></button>'
    }],
    "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result
                var monTotal = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

          var tueTotal = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                var wedTotal = api
                    .column( 6 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

           var thuTotal = api
                    .column( 7 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );



                // Update footer by showing the total with the reference of the column index
          $( api.column( 0 ).footer() ).html('Total');
                $( api.column( 4 ).footer() ).html(monTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $( api.column( 5 ).footer() ).html(tueTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $( api.column( 6 ).footer() ).html(wedTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $( api.column( 7 ).footer() ).html(thuTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

            },
                "paging": false
                });
                $('#table tbody').on( 'click', '.delete', function () {
          table
              .row( $(this).parents('tr') )
              .remove()
              .draw();
      } );
      $("#create").click(function(){
        var id = table.columns(1).data();
        console.log(id[0]);
        var s ={
          "id":id[0],
          "cmp":$("#cmp_s").val(),
          "sup_code":$("#sup_code_s").val()
        }
        console.log(s);
if(id[0]==''){
  alert ("กรุณาเลือกข้อมูลให้ถูกต้อง")
}
  else{      $.ajax({
          type:"POST",
          url :"<?php echo base_url('billingstatement/createStatement')?>",
          data:s,
          success: function(data) {
                    alert("บันทึกข้อมูลสำเร็จ")
                     table.ajax.reload()
                     }

          }
        );
}
});





  })
</script>


<div class="ui equal width grid">

  <div class="column">

  </div>
  <div class="twelve wide column">
    <div class="ui segment">
      <div class="ui form">
        <form class="ui form">
        <div class="fields">

          <div class="six wide field">
            <div class="ui selection dropdown cmp">
              <i class="dropdown icon"></i>
              <input type="hidden" name="cmp"  id="cmp_s">
              <div class="default text">Company</div>
              <div class="menu">
                <div class="item" data-value="">---</div>
                <?php
                foreach ($CompanyAllowed as $value) {
                  echo '<div class="item" data-value="'.$value.'">'.$value.'</div>';
                }?>
              </div>
            </div>
          </div>
          <div class="ten wide field">
            <div class="ui search">
              <div class="ui left icon input">
                  <input class="prompt" type="text" name="Supplier" id="Supplier_s" placeholder="Search Supplier">
                <input type="hidden" name="sup_code" value="" id="sup_code_s">
                 <input type="hidden" name="sup_code" value="" id="Supplier_name_s">

                <i class="address card icon"></i>
              </div>
            </div>
          </div>
          <div class="four wide field">
          <div class="ui calendar month" >
            <div class="ui left icon input">
              <i class="calendar alternate outline icon"></i>
              <input type="text" name="month" id="month_s" aria-describedby="helpId" placeholder="เดือน" value="">
            </div>
          </div>
          </div>
          <div class="field">
            <button id="search" class="ui  icon green button " onclick="return false"><i class="Search icon"></i></button>
          </div>
          <div class="field">
            <button type="reset" id="reset" class="ui  icon red button  "><i class="cut icon"></i></button>
          </div>

        </div>
      </form>
      </div>
    </div>
  </div>
  <div class="column">

  </div>


</div>
<div class="ui equal width grid">

  <div class="column">

  </div>
  <div class="twelve wide column">
    <div class="ui segment">
    <table id="table" class="ui black celled table" cellspacing="0" width="100%">
      <tr>
        <td>
        </td>
      </tr>
      <thead>
        <tr>
          <th>ลำดับ</th>
          <th>ID</th>
          <th>Inv. No.</th>
          <th>Inv. Date</th>
          <th>จำนวนเงิน</th>
          <th>Vat 7%</th>
          <th>WHT%</th>
          <th>จำนวนเงินจ่าย</th>
          <th>วันที่จ่าย</th>
          <th></th>

        </tr>
      </thead>
      <tfoot>
           <tr>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>

           </tr>
       </tfoot>

    </table>
    <button id ="create" class="ui labeled icon green button "><i class="Write Square icon"></i>Create</button>
  </div>
</div>
<div class="column">

</div>
</div>
