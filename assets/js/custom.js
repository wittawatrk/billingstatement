  $(document).ready(function(){
  var table = $('#tableQuo').DataTable({
       "ajax": {
         url: "<?php echo base_url("contactreview/getQuotationline")?>",
         type: 'POST',
         data: function ( d ) {
               d.qttno=$("#qttno").val()

           },

       },
       "bFilter": false,
       "bLengthChange": false,
       "bInfo": false,


     });

     var table = $('#table').DataTable({
        "ajax": {
          url: "<?php echo site_url('contactreview/getQuotations') ?>",
          type: 'POST',
          data: function ( d ) {
                d.cmp=$("#cmp").val(),
                d.year=$("#year").val();
            }

        },
        "columnDefs": [{
          "targets": -1,
          "data": null,
          "defaultContent": '<div align="center"><button class="ui labeled icon green button "><i class="Write Square icon"></i>Create</button></div>'
        }],

      });
    $('#table tbody').on('click', 'button', function() {
      var data = table.row($(this).parents('tr')).data();
      // alert( data[0] +"'s salary is: "+ data[ 5 ] );
      window.location.href = "<?php echo base_url('contactreview/create')?>/" + data[0];
    });
    $('.ui.dropdown')
      .dropdown();


    $("#savebtn").click(function() {
         table.ajax.reload();

              });

  $("#choice1").change(function() {
    if ($(this).is(':checked')) {
      $("#dc1").removeAttr("disabled");
    } else {
      $("#dc1").attr("disabled", "disabled").val('');
    }
  });
  $("#choice2").change(function() {
    if ($(this).is(':checked')) {
      $("#dc2").removeAttr("disabled");
    } else {
      $("#dc2").attr("disabled", "disabled").val('');
    }
  });
  $("#choice3").change(function() {
    if ($(this).is(':checked')) {
      $("#dc3").removeAttr("disabled");
    } else {
      $("#dc3").attr("disabled", "disabled").val('');
    }
  });
  $("#choice4").change(function() {
    if ($(this).is(':checked')) {
      $("#dc4").removeAttr("disabled");
    } else {
      $("#dc4").attr("disabled", "disabled").val('');
    }
  });
  $("#choice5").change(function() {
    if ($(this).is(':checked')) {
      $("#dc5").removeAttr("disabled");
    } else {
      $("#dc5").attr("disabled", "disabled").val('');
    }
  });
  $("#choice6").change(function() {
    if ($(this).is(':checked')) {
      $("#dc6").removeAttr("disabled");

    } else {
      $("#dc6").attr("disabled", "disabled").val('');
    }
  });

  $('.ui.selection.dropdown').dropdown({
    onChange : function(){
        console.log($("#qt").val());
        if($("#qt").val()==1){
          $("#qtno").show();
          $("#cause").hide()
        }
        else{
          $("#qtno").hide();
          $("#cause").show();
        }
      }
    });
  /*   $.ajax({

       url: "?php echo base_url("contactreview/getQuotationline")?>",

       type: "POST",

       data: 'qttno=' + $("#qttno").val(),

       success: function(result) {
         alert(result)
  },
})*/
$('.ui.calendar').calendar({
  type: 'date'
});

});

$(function(){
  $('.ui.sidebar').first()
.sidebar('attach events', '.open.button', 'show')
;
$('.open.button')
.removeClass('disabled')
;
$('.ui.radio.checkbox')
.checkbox()
;
$('.ui.search.selection.dropdown')
.dropdown({
  onChange:function(){
    loadDetail();
        }
});
function loadDetail() {
$.ajax({

  url: "<?php echo base_url("contactreview/getPersonDetail")?>",

  type: "POST",

  data: 'ps=' + $("#ps").val(),

  success: function(result) {

    var obj = jQuery.parseJSON(result);


    $("#tel").val(obj.cnt_f_tel);
    $("#mobile").val(obj.cnt_f_mobile);
    $("#fax").val(obj.cnt_f_fax);
    $("#mail").val(obj.cnt_email);
  }



});
}
});

$(document).ajaxStart(function(){
$('.dimmable').dimmer('show');
});

$(document).ajaxComplete(function(){
$('.dimmable').dimmer('hide');
});
      
