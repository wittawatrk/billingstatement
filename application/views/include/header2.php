<html>

<head>
  <meta charset="utf-8">
  <title></title>
  <link href="<?php echo base_url('assets/css/font-awesome/css/font-awesome.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/semantic/dist/semantic.min.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/semantic-ui-calendar/dist/calendar.min.css') ?>" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/se/dt-1.10.16/b-1.5.1/datatables.min.css" />

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/se/jq-3.2.1/dt-1.10.16/b-1.5.1/datatables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/se/jq-3.2.1/dt-1.10.16/b-1.5.1/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/rowgroup/1.0.2/js/dataTables.rowGroup.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

  <script src="<?php echo base_url('assets/semantic/dist/semantic.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/semantic-ui-calendar/dist/calendar.min.js') ?>"></script>


</head>
