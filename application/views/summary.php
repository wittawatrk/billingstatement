<script type="text/javascript">
  $(document).ready(function(){
    var table = $('#table').DataTable({
            "ajax": {
              url: "<?php echo site_url('billingstatement/getdatatoreport') ?>",
              type: 'POST',
              data:function(d) {
                d.cmp=$("#cmp_s").val(),
                d.dstart=$('#month_start').val(),
                d.dend=$('#month_end').val()
              }

            },
            "ordering": false,
            order: [[1, 'asc']],
            dom: 'Bfrtip',
            buttons: [
              { extend: 'print', footer: true },
        { extend: 'excelHtml5', footer: true },
                  ],
                "searching": false,
            rowGroup: {
              startRender:function(rows,group){
                var avg = rows
                    .data()
                    .pluck(2)

                return group+'       '+avg[0];

              },
           endRender: function ( rows, group ) {
             var intVal = function ( i ) {
                 return typeof i === 'string' ?
                     i.replace(/[\$,]/g, '')*1 :
                     typeof i === 'number' ?
                         i : 0;}
               var avg = rows
                   .data()
                   .pluck(10)
                   .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                   }, 0) ;

               return 'ยอดรวมจ่ายทั้งสิ้น: '+avg.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+' บาท';

           },
           dataSrc: 1
       },"aoColumnDefs": [ {
    "aTargets": [ 10,7,8,9 ],
  "mRender": function (data, type, full) {
   var formmatedvalue=data.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return formmatedvalue;
  }

},{"visible": false, "targets":[ 1,2]}],
        "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // converting to interger to find total
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // computing column Total of the complete result
                    var monTotal = api
                        .column( 7 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

              var tueTotal = api
                        .column( 8 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    var wedTotal = api
                        .column( 9 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

               var thuTotal = api
                        .column( 10 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );



                    // Update footer by showing the total with the reference of the column index
              $( api.column( 0 ).footer() ).html('Total');
                    $( api.column( 7 ).footer() ).html((Number(monTotal).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                    $( api.column( 8 ).footer() ).html((Number(tueTotal).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                    $( api.column( 9 ).footer() ).html((Number(wedTotal).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                    $( api.column( 10 ).footer() ).html((Number(thuTotal).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

                },
                "paging": false,

            });

              $(' .ui.selection.dropdown.cmp').dropdown().change(function(){
                table.ajax.reload();
              })

              $('.ui.calendar.month').calendar({
                type: 'date',
                      formatter: {
               date: function (date, settings) {
                 if (!date) return '';
                 var day = date.getDate();
                 var month = date.getMonth() + 1;
                 var year = date.getFullYear();
                 return  + year+ '-' + month + '-'+day  ;
               }},
               onChange: function () {

                 table.ajax.reload();

    },


           });
$("#search").click(function(){
  table.ajax.reload();
})



  })
</script>










<div class="ui equal width grid">

  <div class="column">

  </div>
  <div class="twelve wide column">
    <div class="ui segment">
      <div class="ui form">
        <form class="ui form">
        <div class="fields">

          <div class="six wide field">
            <div class="ui selection dropdown cmp">
              <i class="dropdown icon"></i>
              <input type="hidden" name="cmp"  id="cmp_s">
              <div class="default text">Company</div>
              <div class="menu">
                <div class="item" data-value="">--Select--</div>
                <?php
                foreach ($CompanyAllowed as $value) {
                  echo '<div class="item" data-value="'.$value.'">'.$value.'</div>';
                }?>
              </div>
            </div>
          </div>
          <div class="four wide field">
            <div class="ui calendar month" >
              <div class="ui left icon input">
                <i class="calendar alternate outline icon"></i>
                <input type="text" name="month" id="month_start" aria-describedby="helpId" placeholder="เดือน" value="">
              </div>
            </div>
          </div>
          <div class="four wide field">
          <div class="ui calendar month" >
            <div class="ui left icon input">
              <i class="calendar alternate outline icon"></i>
              <input type="text" name="month" id="month_end" aria-describedby="helpId" placeholder="เดือน" value="">
            </div>
          </div>
          </div>
          <div class="field">
            <button id="search" class="ui  icon green button " onclick="return false"><i class="Search icon"></i></button>
          </div>
          <div class="field">
            <button type="reset" id="reset" class="ui  icon red button  "><i class="cut icon"></i></button>
          </div>

        </div>
      </form>
      </div>
    </div>
  </div>
  <div class="column">

  </div>


</div>
<div class="ui equal width grid">

  <div class="column">

  </div>
  <div class="twelve wide column">
    <div class="ui segment">
    <table id="table" class="ui black celled table testing" cellspacing="0" width="100%" >
      <tr>
        <td>
        </td>
      </tr>
      <thead>
        <tr>
          <th>ลำดับ</th>
          <th>เลขที่</th>
          <th>Sup.Name</th>
          <th>Inv.No.</th>
          <th>Inv.date</th>
          <th>Type</th>
          <th>Credit</th>
          <th>ยอดก่อนจ่าย</th>
          <th>vat</th>
          <th>WHT</th>
          <th>ยอดรวมจ่าย</th>

        </tr>
      </thead>
      <tfoot>
           <tr>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
                <th></th>
                  <th></th>


           </tr>
       </tfoot>

    </table>

  </div>
</div>
<div class="column">

</div>
</div>
