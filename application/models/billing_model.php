<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Billing_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here


        //  $this->db=$this->load->database();
         $this->db2 = $this->load->database('local', TRUE);
        $this->db3 = $this->load->database('user', true);
       // $this->db4 = $this->load->database('test', true);
    }

    public function getpic($data)
    {
        foreach ($data['personDetail'] as $key => $value) {
            # code...
            $val[$key] = $value;
        }
        $sql = "select picfile from User where CitizenID = ";
        $sql .= "'" . $val['CitizenID'] . "'";
        $query = $this->db3->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = '';
        }
        $query->free_result();
        return $data;
    }
    public function getsupplier($cmp){

          $row=$this->db->query("select cicmpy.crdcode as description,'['+RTRIM(LTRIM(cicmpy.crdcode))+']'+convert(nvarchar(50) ,cicmpy.cmp_name)as title,convert(nvarchar(50) ,cicmpy.cmp_name)as name from cicmpy  where cicmpy.cmp_type = 's' and cicmpy.cmp_status = 'A' and cmp ='".$cmp."' ");


        if ($row->num_rows()>0){

            $data=$row->result_array();
          return $data;
        }
        $row->free_result();
    }

    public function createnewdata($data){

      $this->db->insert('BillingStatement.dbo.billCollect',$data);

    }

    public function getAllList($cmp,$sup_code,$month,$CompanyAllowed){
      $sql= "SELECT *,convert(nvarchar(50),case  when type= 1 then 'ค่าสินค้า' when type=2 then 'ค่าบริการ' when type=3 then 'ค่าเช่า'  when type=4 then 'ค่่าขนส่ง' end )as type1  from billingstatement.dbo.billCollect where mapval = 0 and show=1 and cmp in $CompanyAllowed" ;
      if($cmp!=""){
        $sql.="  and cmp = '$cmp'";
      }
      if( $sup_code!= ""  ){
        $sql.="  and sup_code = '$sup_code'";
      }
      if($month!="" ){
        $sql .= "and (  month(month) = month('$month') and year(month) = year('$month')  )";
      }
      $sql.= " order by createdate desc";
      $row =  $this->db->query($sql);
        return $row;
      // if ($row->num_rows()>0){
      //
      //   $row =  $data=$row->result();
      //
      // }
      // else {
      //       $data = '';
      //   }
      //     return $data;
      $row->free_result();
    }
    public function deleteinvoice($id){
      $this->db->query("update billingstatement.dbo.billCollect set show=0 where id=$id");
    }
    public function getbilldata($id){
      $row = $this->db->query("select * from billingstatement.dbo.billCollect where id = $id ");
      return $row;
    }
    public function updateinvoice($id,$data){
      $this->db->where('id',$id);
      $this->db->update('billingstatement.dbo.billCollect',$data);

    }
    public function createBillingstatement($cmp,$mapid,$sup_code){
      $date = date_create(date("Y-m-d H:i:s"));
      $query = $this->db->query("SELECT max (runningid)+1 as id from billingstatement.dbo.billingStatement where yearid = '".date_format($date,'y')."' and monthid = '".date_format($date,'m')."' and cmp like '%$cmp%'");
      if(!empty($query)){
      foreach( $query->result() as $val){
        $id = $val->id;
        $id=!empty($id)?$id:1;
      }}
      else{
       $id=1;}

      $query->free_result();
      $date1 = date_create(date("Y-m-d H:i:s"));

       $this->db->query("insert into billingstatement.dbo.billingStatement values('$cmp','".date_format($date1,'y')."','".date_format($date1,'m')."','".sprintf("%03d",$id)."','$sup_code','".date_format($date1,'Y-m-d H:i:s')."');SELECT SCOPE_IDENTITY() as id");
       $query =$this->db->query("SELECT SCOPE_IDENTITY() as id");


       foreach ($query->result() as $value) {
        $iddata = $value->id;
              }
      foreach ($mapid as $map) {
        $this->db->query("update billingstatement.dbo.billCollect set mapval =1 ,mapid = $iddata where id = $map ");
                      }

    }

    public function getallstatement($month,$sup_code,$cmp,$CompanyAllowed)
    {
      $sql = "SELECT distinct bsm.id,bsm.cmp,bsm.yearid,bsm.monthid,bsm.runningid,bsm.sup_code,bsm.createddate,bc.Supplier,bc.month";
      $sql.= " FROM BillingStatement.dbo.billingStatement bsm  inner join BillingStatement.dbo.billCollect bc on bc.mapid = bsm.id where bsm.cmp in $CompanyAllowed  ";

      if($month != '') $sql .= "and (  month(bc.month) = month('$month') and year(bc.month) = year('$month')  )";
      if($sup_code != '') $sql .= " and bsm.sup_code ='$sup_code' ";
      if($cmp != '') $sql .= " and bsm.cmp ='$cmp' ";
        $sql.="  order by bsm.createddate desc";

      return $this->db->query($sql);

    }
    public function getdataToprint($id){
      $sql= "SELECT *,bc.cmp+'-'+bc.yearid+bc.monthid+bc.runningid as code,convert(nvarchar(50),case  when type= 1 then 'ค่าสินค้า' when type=2 then 'ค่าบริการ' when type=3 then 'ค่าเช่า'  when type=4 then 'ค่่าขนส่ง' end )as type1  from billingstatement.dbo.billCollect";
      $sql.=" inner join BillingStatement.dbo.billingStatement bc on billCollect.mapid = bc.id  where  mapid = $id  ";
      return $this->db->query($sql);
    }
    public function deletestatmentlist($id){
      $this->db->query("update billingstatement.dbo.billCollect set mapval =0  where mapid = $id");
      $this->db->query("delete from BillingStatement.dbo.billingStatement where id = $id");
    }
    public function getAllListtoedit($id){
        $sql= "SELECT *,convert(nvarchar(50),case  when type= 1 then 'ค่าสินค้า' when type=2 then 'ค่าบริการ' when type=3 then 'ค่าเช่า'  when type=4 then 'ค่่าขนส่ง' end )as type1  from billingstatement.dbo.billCollect where mapval = 1 and mapid = $id  " ;
        return $this->db->query($sql);
    }

    public function unmapval($id,$mapid){
      $valid = '(';
      foreach ($id as  $value) {
        $valid .= $value.',';
      }
        $valid .= '0)';
      $this->db->query("update BillingStatement.dbo.billCollect set mapval = 0 ,mapid=NULL where mapid =$mapid and id not in $valid");
    }
    public function getdatatoreport($cmp,$s_month,$e_month){
      $sql = "SELEct * ,convert(nvarchar(50),case  when type= 1 then 'ค่าสินค้า' when type=2 then 'ค่าบริการ' when type=3 then 'ค่าเช่า'  when type=4 then 'ค่่าขนส่ง' end )as type1, bst.cmp+'-'+bst.yearid+bst.monthid+bst.runningid as grouping from BillingStatement.dbo.billCollect bc";
      $sql .="  inner join BillingStatement.dbo.billingStatement bst on bst.id = bc.mapid where bc.cmp = '$cmp' and month >=  '$s_month' and month <= '$e_month' order by grouping asc"   ;
        return $this->db->query($sql);
    }
}
