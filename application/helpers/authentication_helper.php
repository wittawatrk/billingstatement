<?php

defined('BASEPATH') OR exit('No direct script access allowed.');

function authentication() {
    
    $CI = & get_instance();
    $user = $CI->session->userdata('name');
    $login = $CI->session->userdata('login');
    if ($user == '' || $login == '') {
        $CI->session->sess_destroy();      
        redirect("login" ,"refresh");
    }
}
?>