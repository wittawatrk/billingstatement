<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include(APPPATH.'libraries/TCPDF/TCPDF.php');
class printDoc extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $sso   = new SSO();
    $this->session = $sso->getAuthentication();
    $this->load->model('billing_model');
    $this->load->library('PDF');
    date_default_timezone_set("Asia/Bangkok");

    //Codeigniter : Write Less Do More
  }

  function index($id)
  {
     $arr_month = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
     $arr_cmp = array('PEM'=>'Precise Electric Manufacturing Co.,Ltd.',
     'PMW'=>'Precise Electro-Mechanical Works Co., Ltd.',
     'CI'=>'Coelme International Co., Ltd.',
     'PSP'=>'Precise System and Project Co., Ltd.',
     'PSL'=>'Precise Smart Life Co., Ltd.',

     'PPD'=>'Pacific Power Development Corporation Co., Ltd.',
     'PDE'=>'Precise Digital Economy Co.,Ltd.',
     'PC'=>'Precise Corporation Co., Ltd.',
     'PEM1'=>'Precise Electric Manufacturing Co.,Ltd. (Branch No. 01)',
);


     $row = $this->billing_model->getdataToprint($id);

     $pdf = $this->pdf;
     foreach ($row->result() as $key => $value) {
       $cmp = $value->cmp;
       $credit = $value->credit;
       $sup_name = $value->Supplier;
       $duedate = $value->month;
       $code = $value->code;

     }
        $date = date_create($duedate);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);
        $pdf->SetFont('', '', 16);
        $pdf->AddPage();

        $breakMargin = $pdf->getBreakMargin();
        $autoPageBreak = $pdf->getAutoPageBreak();
        $pdf->SetXY($pdf->GetX() + 75, $pdf->GetY() -10);
        $pdf->SetFont('', 'B', 20);

        $pdf->Cell(125, $pdf->CELL_H, "-ใบวางบิล/ใบตั้งเบิกจ่าย-",  0, 1, "L");
        $pdf->SetFont('', 'B', 16);
          $pdf->SetXY($pdf->GetX() + 10, $pdf->GetY()+ 5);
         $pdf->MultiCell(100,1, $arr_cmp[$cmp], 0, 'L', 0, 0, '', '', true);
         $pdf->SetX($pdf->GetX() -28);
         $pdf->MultiCell(100, 1, $code, 0, 'R', 0, 1, '', '', true);
         $pdf->SetXY($pdf->GetX() + 10, $pdf->GetY()+2 );
         $pdf->MultiCell(100,1, "DUE DATE: ".date_format($date,"d")."  " .$arr_month[(int)date_format($date,"m")]."  ".((int)date_format($date,"Y")), 0, 'L', 0, 0, '', '', true);
          $pdf->SetX($pdf->GetX() -28);
         $pdf->MultiCell(100, 1, "DATE: " .date("d/m/Y"), 0, 'R', 0, 1, '', '', true);
        $pdf->SetXY($pdf->GetX() + 10, $pdf->GetY()+1);
        $pdf->Cell(150, $pdf->CELL_H,"NAME: ".$sup_name, 0, 1, "L");
        $pdf->SetXY($pdf->GetX() + 10, $pdf->GetY()+1 );
        $pdf->Cell(40, $pdf->CELL_H,"CREDIT: ".$credit ." วัน", 0, 1, "L");
          $file = fopen(dirname(__FILE__) . "/doc/test.html", "r");
          $html = fread($file, filesize(dirname(__FILE__) . "/doc/test.html"));
          $html = mb_eregi_replace("\>\s+\<", "><", $html);
          $html = mb_eregi_replace("\>\s+\<", "><", $html);
          $pdf->SetFont('', '', 14);
          $pdf->SetXY($pdf->getLMargin()+10, 55);
          $i=0;
          $sumbf=0;
          $sumvat=0;
          $sumwht=0;
          $sumaf =0;

          foreach ($row->result() as $key => $value) {
            $i++;
            $sumbf=$sumbf+$value->bfvat;
            $sumvat=$sumvat+$value->vat;
            $sumwht=$sumwht+$value->wht;
            $sumaf =$sumaf+$value->afvat;
            $html .= "<tr><td>$i</td>";
            $html .= "<td>$value->invoiceno</td>";
            $html .= "<td>".date_format(date_create($value->invoicedate),("d/m/Y"))."</td>";
            $html .= "<td  style=".'"text-align:right;"'." >".number_format($value->bfvat,2)."</td>";
            $html .= "<td  style=".'"text-align:right;"'." >".number_format($value->vat,2)."</td>";
            $html .= "<td  style=".'"text-align:right;"'." >".number_format($value->wht,2)."</td>";
            $html .= "<td  style=".'"text-align:right;"'." >".number_format($value->afvat,2)."</td>";
            $html .= "<td>".date_format(date_create($value->month),("d/m/Y"))."</td></tr>";

    # code...
  }
  $html .= '<tr><td colspan = "3">Total</td>';
  $html .= "<td  style=".'"text-align:right;"'." >".number_format($sumbf,2)."</td>";
  $html .= "<td  style=".'"text-align:right;"'." >".number_format($sumvat,2)."</td>";
  $html .= "<td  style=".'"text-align:right;"'." >".number_format($sumwht,2)."</td>";
  $html .= "<td  style=".'"text-align:right;"'." >".number_format($sumaf,2)."</td>";
  $html .= "<td></td></tr>";
  $html.= "</table>";
   $pdf->writeHTML($html, true, false, false, false, '');
   $pdf->SetXY($pdf->GetX() + 10, $pdf->GetY()+3 );
     $pdf->MultiCell(100,1, "ผู้วางบิล ......................................", 0, 'L', 0, 0, '', '', true);
     $pdf->SetX($pdf->GetX() -25);
   $pdf->MultiCell(100, 1, "ผู้ตรวจสอบ......................................", 0, 'R', 0, 1, '', '', true);
  $pdf->Output("ProductReceipt.pdf");

  }

}
