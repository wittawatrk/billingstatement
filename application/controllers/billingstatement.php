
<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class billingstatement extends CI_Controller
{
        function __construct()
    {
        parent::__construct();
        $sso   = new SSO();
        $this->session = $sso->getAuthentication();
        $this->load->model('billing_model');
        date_default_timezone_set("Asia/Bangkok");

        //Do your magic here

    }


    public function index(){
        $data['user']  = $this->session;
        $data['CompanyAllowed']= $data['user']['personDetail']['CompanyAllowed'];
        $data['pic']   = $this->billing_model->getpic($this->session);
        $data['title'] = "เพิ่มรายการ Invoice";
        $this->load->view('include\header2');
        $this->load->view('include\sidebar', $data);
        $this->load->view('main');

    }
    public function getsupplier(){

        $data=$this->billing_model->getsupplier($this->input->post('cmp'));
        $value=array();
        foreach ($data as $key=>$val) {

          $dat[]=array('title'=>$val['title'],
                        'ID'=>$val['description'],
                        'name'=>$val['name']
                      );
          # code...
        }

        echo json_encode($dat);
    }

    public function createnewinvoice(){
      $data  = array( "cmp"=>$this->input->post( "cmp"),
      "invoiceno"=>$this->input->post( "invoiceno"),
      "invoicedate"=>$this->input->post("invoicedate"),
      "type"=>$this->input->post("type"),
      "sup_code"=>$this->input->post("sup_code"),
      "Supplier"=>$this->input->post("Supplier"),
      "credit"=>$this->input->post("credit"),
      "bfvat"=>$this->input->post("bfvat"),
      "wht"=>$this->input->post("wht"),
      "vat"=>$this->input->post("vat"),
      "afvat"=>$this->input->post("afvat"),
      "month"=>$this->input->post("month"),
      "createdate"=> date("Y-m-d H:i:s"),
      "mapval" => 0,
      "show"=>1,
 );
 $this->billing_model->createnewdata($data);

 echo json_encode(array('success'=>'บันทึกข้อมูลสำเร็จ.'));
    }

    public function getallList(){
      $CompanyAllowed = '(';

       foreach ($this->session['personDetail']['CompanyAllowed'] as $value) {
         $CompanyAllowed .= "'".$value."',";
         # code...
       }
       $CompanyAllowed.="'')";
       $draw      = intval($this->input->get("draw"));
      $alllist =$this->billing_model->getAllList($this->input->post('cmp'),$this->input->post('sup_code'),$this->input->post('month'),$CompanyAllowed);
  //    print_r($alllist->result());
      if($alllist->num_rows()>0){
        $i=0;
      foreach ($alllist->result() as $value) {
      $date = date_create($value->month);
        $data[]=array(
          ++$i,
          $value->id,
          $value->invoiceno,
          $value->invoicedate,
          $value->type1,
          $value->Supplier,
          $value->credit,
          $value->bfvat,
          $value->vat,
          $value->wht,
          $value->afvat,
          !$value->month?null:date_format($date,'M'),
          $value->month,
          $value->cmp
        );
            }
            $output = array(
                "draw" => $draw,
                "recordsTotal" => $this->billing_model->getAllList('','','',$CompanyAllowed)->num_rows(),
                "recordsFiltered" => $alllist->num_rows,
                "data" => $data
            );
       }
            else{
              $output = array(
                      "draw" => $draw,
                      "recordsTotal" => 0,
                      "recordsFiltered" => 0,
                      "data" => ''
                  );
            }
            echo json_encode($output);
    }
    public function getallListtocreate(){
      $CompanyAllowed = '(';

       foreach ($this->session['personDetail']['CompanyAllowed'] as $value) {
         $CompanyAllowed .= "'".$value."',";
         # code...
       }
       $CompanyAllowed.="'')";
       $draw      = intval($this->input->get("draw"));
      $alllist =$this->billing_model->getAllList($this->input->post('cmp'),$this->input->post('sup_code'),$this->input->post('month'),$CompanyAllowed);
  //    print_r($alllist->result());
    if($this->input->post('cmp')==''||$this->input->post('sup_code')==''||$this->input->post('month')==''){
      $output = array(
              "draw" => 1,
              "recordsTotal" => 0,
              "recordsFiltered" => 0,
              "data" => ''
          );
    }
    else
        {
     if($alllist->num_rows()>0){
       $i=0;
      foreach ($alllist->result() as $value) {
        $i++;
      $date = date_create($value->month);
        $data[]=array(
          $i,
          $value->id,
          $value->invoiceno,
          $value->invoicedate,

          $value->bfvat,
          $value->vat,
          $value->wht,
          $value->afvat,

          $value->month

        );
            }
            $output = array(
                "draw" => $draw,
                "recordsTotal" => $this->billing_model->getAllList('','','',$CompanyAllowed)->num_rows(),
                "recordsFiltered" => $alllist->num_rows,
                "data" => $data
            );
       }
            else{
              $output = array(
                      "draw" => $draw,
                      "recordsTotal" => 0,
                      "recordsFiltered" => 0,
                      "data" => ''
                  );
            }
          }

            echo json_encode($output);
    }

    public function deleteinvoice(){
      $this->billing_model->deleteinvoice($this->input->post('id'));
       echo json_encode(array('success'=>'ลบข้อมูลสำเร็จ.'));
    }
    public function getbilldata(){
      $data = $this->billing_model->getbilldata($this->input->post('id'));
      echo json_encode($data->result());
    }

    public function updateinvoice(){
      $data  = array( "cmp"=>$this->input->post( "cmp"),
      "invoiceno"=>$this->input->post( "invoiceno"),
      "invoicedate"=>$this->input->post("invoicedate"),
      "type"=>$this->input->post("type"),
      "sup_code"=>$this->input->post("sup_code"),
      "Supplier"=>$this->input->post("Supplier"),
      "credit"=>$this->input->post("credit"),
      "bfvat"=>$this->input->post("bfvat"),
      "wht"=>$this->input->post("wht"),
      "vat"=>$this->input->post("vat"),
      "afvat"=>$this->input->post("afvat"),
      "month"=>$this->input->post("month"),
    );
    $id = $this->input->post("id");
    $this->billing_model->updateinvoice($id,$data);
echo json_encode(array('success' => "แก้ไขข้อมูลสำเสร็จ" ));
    }


    public function createbilling(){
      $data['user']  = $this->session;
      $data['CompanyAllowed']= $data['user']['personDetail']['CompanyAllowed'];
      $data['pic']   = $this->billing_model->getpic($this->session);
      $data['title'] = "สร้างใบวางบิล";
      $this->load->view('include\header2');
      $this->load->view('include\sidebar', $data);
      $this->load->view('createBilling');
    }
    public function createStatement(){
      $this->billing_model->createBillingstatement($this->input->post('cmp'),$this->input->post('id'),$this->input->post('sup_code'));

    }
    public function statementList(){
      $data['user']  = $this->session;
      $data['CompanyAllowed']= $data['user']['personDetail']['CompanyAllowed'];
      $data['pic']   = $this->billing_model->getpic($this->session);
      $data['title'] = "รายการใบวางบิล";

      $this->load->view('include\header2');
      $this->load->view('include\sidebar', $data);
      $this->load->view('statementList');
    }

    public function getStatementList(){
       $draw      = intval($this->input->get("draw"));
      $CompanyAllowed = '(';

       foreach ($this->session['personDetail']['CompanyAllowed'] as $value) {
         $CompanyAllowed .= "'".$value."',";
         # code...
       }
       $CompanyAllowed.="'')";
       $row=$this->billing_model->getallstatement($this->input->post('month'),$this->input->post('sup_code'),$this->input->post('cmp'),$CompanyAllowed);

       if($row->num_rows()>0){
         $i=0;
        foreach ($row->result() as $value) {
          $i++;
        $date = date_create($value->month);
          $data[]=array(
            $i,
            $value->id,
            $value->cmp,
            $value->cmp."-".$value->yearid.$value->monthid.$value->runningid,
            $value->Supplier,
            date_format($date,"d/m/Y"),

          );
              }
              $output = array(
                  "draw" => 0,
                  "recordsTotal" => $this->billing_model->getallstatement('','','',$CompanyAllowed)->num_rows(),
                  "recordsFiltered" => $row->num_rows,
                  "data" => $data
              );
         }
              else{
                $output = array(
                        "draw" => $draw,
                        "recordsTotal" => 0,
                        "recordsFiltered" => 0,
                        "data" => ''
                    );
              }
              echo json_encode($output);
    }
    public function deletestatmentlist(){
      $this->billing_model->deletestatmentlist($this->input->post('id'));
      echo json_encode("success");
    }
    public function getAllListtoedit(){
      $draw      = intval($this->input->get("draw"));
      $alllist=$this->billing_model->getAllListtoedit($this->input->post('id'));
      if($alllist->num_rows()>0){
        $i=0;
       foreach ($alllist->result() as $value) {
         $i++;
       $date = date_create($value->month);
         $data[]=array(
           $i,
           $value->id,
           $value->invoiceno,
           $value->invoicedate,

           $value->bfvat,
           $value->vat,
           $value->wht,
           $value->afvat,

           $value->month

         );
             }
             $output = array(
                 "draw" => $draw,
                 "recordsTotal" => $this->billing_model->getAllListtoedit($this->input->post('id'))->num_rows(),
                 "recordsFiltered" => $alllist->num_rows,
                 "data" => $data
             );
        }
             else{
               $output = array(
                       "draw" => $draw,
                       "recordsTotal" => 0,
                       "recordsFiltered" => 0,
                       "data" => ''
                   );
             }


             echo json_encode($output);
    }
public function editstatement(){
  $data['title'] = "แก้ไขใบใบวางบิล";
  $data['user']  = $this->session;
  $data['CompanyAllowed']= $data['user']['personDetail']['CompanyAllowed'];
  $data['pic']   = $this->billing_model->getpic($this->session);
  $data['id'] = ($this->uri->segment(3) != '') ? $this->uri->segment(3) : '';
  $this->load->view('include\header2');
  $this->load->view('include\sidebar', $data);
  $this->load->view('editbill');

}
public function editStatementlist(){
  $this->billing_model->unmapval($this->input->post('id'),$this->input->post('mapid'));
   echo json_encode(array('success'=>'แก้ไขข้อมูลสำเร็จ.'));
}
public function getdatatoreport(){
   $draw      = intval($this->input->get("draw"));
  $CompanyAllowed = '(';

   foreach ($this->session['personDetail']['CompanyAllowed'] as $value) {
     $CompanyAllowed .= "'".$value."',";
     # code...
   }
   $CompanyAllowed.="'')";

if(!$this->input->post('cmp')||!$this->input->post('dstart')||!$this->input->post('dend')){
  $output = array(
          "draw" => $draw,
          "recordsTotal" => 0,
          "recordsFiltered" => 1,
          "data" => '');
}
else{
  $row=$this->billing_model->getdatatoreport($this->input->post('cmp'),$this->input->post('dstart'),$this->input->post('dend'));
   if($row->num_rows()>0){
     $i=0;
    foreach ($row->result() as $value) {
      $i++;
    $date = date_create($value->month);
      $data[]=array(
        $i,
        $value->grouping,
        $value->Supplier,
        $value->invoiceno,
        $value->invoicedate,
        $value->type1,
        $value->credit,
        $value->bfvat,
        $value->vat,
        $value->wht,
        $value->afvat,


      );
          }
          $output = array(
              "draw" => 0,
              "recordsTotal" => $this->billing_model->getdatatoreport($this->input->post('cmp'),$this->input->post('dstart'),$this->input->post('dend'))->num_rows(),
              "recordsFiltered" => $row->num_rows,
              "data" => $data
          );
     }
          else{
            $output = array(
                    "draw" => $draw,
                    "recordsTotal" => 0,
                    "recordsFiltered" => 0,
                    "data" => ''
                );
          }
        }
          echo json_encode($output);
}
public function summaryreport(){
  $data['title'] = "สรุปยอดการจ่ายเงิน";
  $data['user']  = $this->session;
  $data['CompanyAllowed']= $data['user']['personDetail']['CompanyAllowed'];
  $data['pic']   = $this->billing_model->getpic($this->session);
  $data['id'] = ($this->uri->segment(3) != '') ? $this->uri->segment(3) : '';
  $this->load->view('include\header2');
  $this->load->view('include\sidebar', $data);
  $this->load->view('summary');
}

}
