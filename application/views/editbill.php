<script type="text/javascript">
  $(document).ready(function(){
    function loaddata(){
      var myData;
      var s={"cmp":$("#cmp").val()}
      $.ajax({
          type:"POST",
          dataType:'json',
          url :"<?php echo base_url('billingstatement/getsupplier') ?>",
          data:s,

          success:function(data){
            myData = data

          },


          async: false
          });

          return myData;
    }

    $(' .ui.selection.dropdown.cmp').dropdown().change(function(){
  var $result = loaddata()

       $('.ui.search')
         .search({
           source: $result,
           searchFields:[
             'title'
           ],
           fullTextSearch	:'exact',

           onSelect: function (result,response) {
                  $("#sup_code_s").val(result.ID);
                  $("#Supplier_name_s").val(result.name);


                  return true;
         }


      });


    });

    $('.ui.calendar').calendar({
  type: 'date',
  formatter: {
     date: function (date, settings) {
       if (!date) return '';
       var day = date.getDate();
       var month = date.getMonth() + 1;
       var year = date.getFullYear();
       return  year + '-' + month + '-' + day ;
     }}
   });


   $('.ui.calendar.month').calendar({
 type: 'month',
 formatter: {
    date: function (date, settings) {
      if (!date) return '';
      var month = date.getMonth() + 1;
      var year = date.getFullYear();
      return  + year+ '-' + month + '-'+'20'  ;
    }}
});

    $("#search").click(function(){
      table.ajax.reload();
    })
  var table =  $('#table').DataTable({
            "ajax": {
              url: "<?php echo site_url('billingstatement/getAllListtoedit') ?>",
              type: 'POST',
              data: function ( d ) {
                    d.id=$("#id").val()
                    }

            },

            "ordering": false,
            "aoColumnDefs": [ {
         "aTargets": [ 4,5,6,7 ],
       "mRender": function (data, type, full) {
        var formmatedvalue=data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
         return formmatedvalue;
       }
    },{
      "targets": -1,
      "data": null,
      "defaultContent": '<button id="delete" class="ui  icon red button delete "><i class="window close Square icon"></i></button>'
    }],
    "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total of the complete result
                var monTotal = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

          var tueTotal = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                var wedTotal = api
                    .column( 6 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

           var thuTotal = api
                    .column( 7 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );



                // Update footer by showing the total with the reference of the column index
          $( api.column( 0 ).footer() ).html('Total');
                $( api.column( 4 ).footer() ).html((Number(monTotal).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $( api.column( 5 ).footer() ).html((Number(tueTotal).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $( api.column( 6 ).footer() ).html((Number(wedTotal).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                $( api.column( 7 ).footer() ).html((Number(thuTotal).toFixed(2)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

            },
                "paging": false
                });
                $('#table tbody').on( 'click', '.delete', function () {
          table
              .row( $(this).parents('tr') )
              .remove()
              .draw();
      } );
      $("#create").click(function(){
        var id = table.columns(1).data();
        console.log(id[0]);
        var s ={
          "id":id[0],
          "mapid":$("#id").val()   }
        console.log(s);

        $.ajax({
          type:"POST",
          url :"<?php echo base_url('billingstatement/editStatementlist')?>",
          data:s,
          success: function(data) {
                    alert("บันทึกข้อมูลสำเร็จ")
                     table.ajax.reload()
                     }

          }
        );

});




  })
</script>

  <input type="hidden" name="" value="<?=$id?>" id="id">
  <div class="twelve wide column">
    <div class="ui segment">
    <table id="table" class="ui black celled table" cellspacing="0" width="100%">
      <tr>
        <td>
        </td>
      </tr>
      <thead>
        <tr>
          <th>ลำดับ</th>
          <th>ID</th>
          <th>Inv. No.</th>
          <th>Inv. Date</th>
          <th>จำนวนเงิน</th>
          <th>Vat 7%</th>
          <th>WHT%</th>
          <th>จำนวนเงินจ่าย</th>
          <th>วันที่จ่าย</th>
          <th></th>

        </tr>
      </thead>
      <tfoot>
           <tr>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>

           </tr>
       </tfoot>

    </table>
    <button id ="create" class="ui labeled icon green button "><i class="Write Square icon"></i>Save</button>
  </div>
</div>
<div class="column">

</div>
</div>
